
public class Auto {

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getBesitzer() {
		return besitzer;
	}

	public void setBesitzer(String besitzer) {
		this.besitzer = besitzer;
	}

	public int getBaujahr() {
		return baujahr;
	}

	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	public int getHubraum() {
		return hubraum;
	}

	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}

	private String marke;
	private String farbe;

	private String besitzer;
	private int baujahr;
	private int hubraum;

	public Auto() {
		System.out.println("Auto-Objekt");
	}

	public Auto(String marke, String farbe) {
		this.marke = marke;
		this.farbe = farbe;
	}

	public void setMarke(String marke) {
		this.marke = marke;
	}

	public String getMarke() {
		return this.marke;
	}

	// weitere Methoden
	public void fahre(int strecke) {
		System.out.println("Los geht's!!!");
	}

	public void tanke(int liter) {
		System.out.println("Bin getankt");
	}
}
