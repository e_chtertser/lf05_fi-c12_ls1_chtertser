
public class Formatieren {

	public static void main(String[] args) {
//		System.out.printf("%6s%n", "**" );
//		System.out.printf("%-6s%4s%n", "*"  , "*");
//		System.out.printf("%-6s%4s%n", "*"  , "*");
//		System.out.printf("%6s%n", "**" );
//		System.out.printf("%-5s = %-19s = %4d%n","0!","",1);
//		System.out.printf("%-5s = %-19s = %4d%n","0!","1",1);
//		System.out.printf("%-5s = %-19s = %4d%n","0!","1 * 2",2);
//		System.out.printf("%-5s = %-19s = %4d%n","0!","1 * 2 * 3",6);
//		System.out.printf("%-5s = %-19s = %4d%n","0!","1 * 2 * 3 * 4",24);
//		System.out.printf("%-5s = %-19s = %4d%n","0!","1 * 2 * 3 * 4 * 5",120);
		
		System.out.printf("%-12s|%10s%n", "Fahrenheit", "Celsius");
		System.out.printf("------------------------%n");
		System.out.printf("%-12d|%10.2f%n", -20, -28.8889);
		System.out.printf("%-12d|%10.2f%n", -10, -23.3333);
		System.out.printf("+%-11d|%10.2f%n", 0, -17.7778);
		System.out.printf("+%-11d|%10.2f%n", 20, -6.6667);
		System.out.printf("+%-11d|%10.2f%n", 30, -1.1111);
		System.out.print(false);
		

	}

}
