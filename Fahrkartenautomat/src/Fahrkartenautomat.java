﻿
import java.util.Scanner;

public class Fahrkartenautomat{
	
    public static void main(String[] args)
    {
   	
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       
       do {
    	   //Bestellung erfassen
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	   //Eingezahlen Betrag erfassen
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    	   //Fahrkarte drucken
    	   fahrkartenAusgeben();
    	   //Rückgeld geben
    	   rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
       
    	   warte(3450); //delay, bevor eine neue Bestellung aufgenommen wird in ms \\
       }
       while(true);
       
    }
    
    
    private static void warte(int milisecond){
    	try {
    		Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);	
    }

    //gibt den Gesamtpreis der Tickets zurück
    private static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	int tickets;
    	boolean ticketInputCheck = true;
    	int ticketChoice = 0;
    	
        //Ticketauswahl
    
    	System.out.println("Wählen Sie ihre Fahrkarte für Berlin AB aus \n" +
    						"  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" +
    						"  Tageskarte Regeltarif AB [8,60 EUR] (2)\n" +
    						"  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n\n");
    		
    	do { //eingabe + überprüfung ob valid
    		ticketChoice = tastatur.nextInt();
    		
    		System.out.println("Ihre Wahl: " + ticketChoice);
    		
    		//checks validity
    		if((ticketChoice>0) && (ticketChoice<4)) 
    			ticketInputCheck = false;
    		else
    			System.out.println(" >>falsche Eingabe<<");
    			
    	}
    	while(ticketInputCheck);
    	

        //Eingabe Anzahl der Tickets
        System.out.print("Anzahl der Tickets: ");   	   
        tickets = tastatur.nextInt();

        if((tickets>10) || (tickets<1)) {
        	//ungültiger Eingabebereich (<1 / >10 Tickets)
        	tickets = 1;
        	System.out.println("Ungültige Eingabe! Bestellung wird für (1) Ticket fortgeführt!");
        }
        
        //berechnung des Ticket Preises, je nach Auswahl
        switch(ticketChoice) {
        	case 1: zuZahlenderBetrag =  2.90 * tickets; break;
        	case 2: zuZahlenderBetrag =  8.60 * tickets; break;
        	case 3: zuZahlenderBetrag = 23.50 * tickets; break;
        }
        

        return zuZahlenderBetrag;
    }
    
    //gibt den eingezahlten Betrag zurück
    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu Zahlender Betrag %.2f Euro \n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    return eingezahlterGesamtbetrag;
    }
    
    //animierte Fahrscheinausgabe
    private static void fahrkartenAusgeben() {
        // Fahrscheinausgabe 
        // -----------------     
     	   System.out.println("\nFahrschein wird ausgegeben");
     	   
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
 			warte(250);
 	       }
        System.out.println("\n");
    	
    }
    
    //Textausgabe, welche Münzen als Rückgeld ausgegeben werden
    private static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f", rückgabebetrag);
     	   System.out.println(" EURO \nwird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	   muenzeAusgeben(1, "EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	   muenzeAusgeben(50, "Cent");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	   muenzeAusgeben(20, "Cent");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	   muenzeAusgeben(10, "Cent");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	   muenzeAusgeben(5, "Cent");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n"+
                           "______________________________________\n\n");
    }
}