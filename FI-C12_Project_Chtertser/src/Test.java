
public class Test {

	public static void main(String[] args) {
		System.out.println("Das ist ein \"Beispielsatz\".\nEin Beispielsatz ist das.");	
	//Das "println" l�sst einen direkt in der n�chsten Zeile schreiben und nur das "print" nicht.
	//Um dennoch in der n�chsten Zeile schreiben zu k�nnen macht man einfach ein \n ab dem Punkt 
	//wo man in der n�chsten Zeile schreiben will.
		System.out.printf("Hello %s!%n", "World");
		//https://moodle.oszimt.de/pluginfile.php/354646/mod_resource/content/2/AB-Konsolenausgabeformatierung.pdf
		//https://moodle.oszimt.de/pluginfile.php/224941/mod_resource/content/6/IB-Konsolenausgabeformatierung.pdf
		//https://moodle.oszimt.de/course/view.php?id=2890
	}

}
