
public class Formatierung{

	public static void main(String[] args) {
//		// Ganze Zahl
//		int zahl = 123456789;
//		System.out.printf("|%-20d|%n", zahl);
		
//		// Komma Zahlen
//		System.out.printf("|%f|%n", 12345.123456789);
//		System.out.printf("|%.2f|%n", 12345.123456789);
//		System.out.printf("|%20.2f|%n", 12345.123456789);
//		System.out.printf("|%-20.2f|%n", 12345.123456789);
//		
//		// Zeichenkette
//		System.out.printf("|%s|%n", "12345789");
//		System.out.printf("|%20s|%n", "12345789");
//		System.out.printf("|%-20s|%n", "12345789");
//		System.out.printf("|%-20.4s|%n", "12345789");
//		System.out.printf("Name:%-10s,Alter:%-10d,Gehalt: %-10.2fEuro", "Max", 18, 1801.50);
		System.out.printf("%-12s|%10s%n", "Fahrenheit", "Celsius");
		System.out.printf("------------------------%n");
		System.out.printf("%-12d|%10.2f%n", -20, -28.8889);
		System.out.printf("%-12d|%10.2f%n", -10, -23.3333);
		System.out.printf("+%-11d|%10.2f%n", 0, -17.7778);
		System.out.printf("+%-11d|%10.2f%n", 20, -6.6667);
		System.out.printf("+%-11d|%10.2f%n", 30, -1.1111);

		
		
	}

}
